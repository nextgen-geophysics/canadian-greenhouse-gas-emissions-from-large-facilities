#!/usr/bin/env python

from io import StringIO
import requests

import pandas as pd

DATA_URL = 'https://www.canada.ca/content/dam/eccc/documents/csv/cesindicators/ghg-emissions/2021/greenhouse-gas-emissions-large-facilities.csv'

# Get data from canada.ca
resp = requests.get(DATA_URL)
if resp.status_code != 200:
    raise RuntimeError('Unable to get dataset with error ', resp.text)

# Read data into Pandas
raw_csv = StringIO(resp.text)
df = pd.read_csv(raw_csv)

# Clean dataset
df.dropna(subset=['Facility name'], inplace=True)

# Cleanup columns
df['Total emissions'] = df['Total emissions'].astype(int)
df['coordinates'] = df.apply(lambda x: [x.Longitude, x.Latitude], axis=1)
df.rename(columns={
    "Facility name": "facility",
    "Total emissions": "total_emissions"
}, inplace=True)
        
# Export to JSON
df[['coordinates', 'total_emissions', 'facility']]\
    .to_json('canada-facilities.json', orient='records', indent=True)
