# Canadian greenhouse gas emissions from large facilities

This repository contains a interactive webmap that displays large-scale emissions facilities in Canada.

**[VIEW MAP](https://canadian-emitters.nextgengeo.com/)**

## References
- [Data source](https://www.canada.ca/en/environment-climate-change/services/environmental-indicators/greenhouse-gas-emissions/large-facilities.html)
